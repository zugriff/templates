import { Hono } from "hono";

const app = new Hono();

app.get("/", (c) => c.text("Hello World!"));

addEventListener("fetch", (event) => event.respondWith(app.fetch(event)));
